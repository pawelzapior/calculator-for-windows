﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Calculator_MVVM.Model
{
    class Calc
    {
        private double value1;
        private double value2;
        private char operand;


        public Calc(double value1, double value2, char operand)
        {
            this.value1 = value1;
            this.value2 = value2;
            this.operand = operand;
        }

        public double Calculate()
        {

                    switch (operand)
                    {
                        case '+':
                            return (double)(value1 + value2);
                        case '-':
                            return (double)(value1 - value2);
                        case '/':
                            if (value2 == 0) MessageBox.Show("Dividing by 0.", "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
                            else return (double)(value1 / value2);
                            break;
                        case '∙':
                            return (double)(value1 * value2);
                        case '%':
                            if (value2 == 0) MessageBox.Show("Dividing by 0.", "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
                            else return (double)((value1 / value2) * 100);
                            break;

                default:
                            break;
                    }
                    return 0.0;
        }



    }
}
