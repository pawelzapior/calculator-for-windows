﻿using Calculator_MVVM.ViewModel.BaseClass;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace Calculator_MVVM.ViewModel
{

    class MainViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        

        List<char> charValue1 = new List<char>();
        List<char> charValue2 = new List<char>();
        bool actVal = false;
        private char charOperator=' ';
        private string output = "";

        public char CharOperator
        {
            get { return charOperator; }

            private set
            {

                charOperator= value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(charOperator)));
            }
        }

        public string Output
        {
            get { return output; }

            private set
            {
               output = value;
               PropertyChanged?.Invoke(this,new PropertyChangedEventArgs(nameof(output)));
            }
        }

       

        private ICommand startCalc;

        public ICommand StartCalc
        {
            get
            {
                return startCalc ?? (startCalc = new BaseClass.RelayCommand(
                    p => 
                    {

                        double v1 = double.Parse(convertToString(charValue1));
                        double v2 = double.Parse(convertToString(charValue2));
                        Model.Calc result = new Model.Calc(v1, v2, charOperator);
                        double r=result.Calculate();
                        charValue1.Clear();
                        charValue2.Clear();

                        actVal = false;
                        char[] tab;
                        tab=r.ToString().ToCharArray();

                        foreach (char el in tab)
                        {
                            AddToInput(el);
                        }



                    },
                    p=>charValue1.Count > 0 && charValue2.Count > 0 && charOperator != ' '

                    )) ;
            }

        }

        private ICommand cancel;

        public ICommand Cancel
        {
            get
            {
                return cancel ?? (cancel = new BaseClass.RelayCommand(
                    p =>
                    {
                        charValue1.Clear();
                        charValue2.Clear();
                        charOperator = ' ';
                        Output = " ";
                        actVal = false;

                    },
                    p => true

                    ));
            }

        }


        public string convertToString(List<char> numbers)
        {
            StringBuilder builder = new StringBuilder();
            foreach (char n in numbers) // Loop through all strings
            {
                builder.Append(n); // Append string to StringBuilder
            }
            string result = builder.ToString(); // Get string from StringBuilder
            return result;
        }

        public void AddToInput(char v)
        {
            if(actVal==false)
            {
                charValue1.Add(v);
                Output = convertToString(charValue1);
            }
            if(actVal==true)
            {
                charValue2.Add(v);
                Output = convertToString(charValue2);
            }


        }






        private ICommand b7;
        public ICommand B7
        {
            get
            {

                return b7 ?? (b7 = new RelayCommand(
                                        p => AddToInput('7'),
                                        p => true
                    ));
            }
        }

        private ICommand b8;
        public ICommand B8
        {
            get
            {

                return b8 ?? (b8 = new RelayCommand(
                                        p => AddToInput('8'),
                                        p => true
                    ));
            }
        }

        private ICommand b9;
        public ICommand B9
        {
            get
            {

                return b9 ?? (b9 = new RelayCommand(
                                        p => AddToInput('9'),
                                        p => true
                    ));
            }
        }

        private ICommand b4;
        public ICommand B4
        {
            get
            {

                return b4 ?? (b4 = new RelayCommand(
                                        p => AddToInput('4'),
                                        p => true
                    ));
            }
        }

        private ICommand b5;
        public ICommand B5
        {
            get
            {

                return b5 ?? (b5 = new RelayCommand(
                                        p => AddToInput('5'),
                                        p => true
                    ));
            }
        }

        private ICommand b6;
        public ICommand B6
        {
            get
            {

                return b6 ?? (b6 = new RelayCommand(
                                        p => AddToInput('6'),
                                        p => true
                    ));
            }
        }

        private ICommand b1;
        public ICommand B1
        {
            get
            {

                return b1 ?? (b1 = new RelayCommand(
                                        p => AddToInput('1'),
                                        p => true
                    ));
            }
        }

        private ICommand b2;
        public ICommand B2
        {
            get
            {

                return b2 ?? (b2 = new RelayCommand(
                                        p => AddToInput('2'),
                                        p => true
                    ));
            }
        }

        private ICommand b3;
        public ICommand B3
        {
            get
            {

                return b3 ?? (b3 = new RelayCommand(
                                        p => AddToInput('3'),
                                        p => true
                    ));
            }
        }

        private ICommand b0;
        public ICommand B0
        {
            get
            {

                return b0 ?? (b0 = new RelayCommand(
                                        p => AddToInput('0'),
                                        p => true
                    ));
            }
        }

        private ICommand coma;
        public ICommand Coma
        {
            get
            {

                return coma ?? (coma = new RelayCommand(
                                        p => AddToInput(','),
                                        p => ((actVal == false && charValue1.Count() > 0)|| (actVal == true && charValue2.Count() > 0)) && ((actVal == false && !(charValue1.Contains(','))) || (actVal == true && !(charValue2.Contains(','))))
                    ));
            }
        }

        private ICommand bPlus;
        public ICommand BPlus
        {
            get
            {

                return bPlus ?? (bPlus = new RelayCommand(
                                        p =>
                                        {
                                            actVal=true;
                                            CharOperator = '+';
                                        },
                                        p => actVal == false && charValue1.Count() > 0

                    )) ;
            }
        }

        private ICommand bMinus;
        public ICommand BMinus
        {
            get
            {

                return bMinus ?? (bMinus = new RelayCommand(
                                        p =>
                                        {
                                            actVal = true;
                                            CharOperator = '-';
                                        },
                                        p => actVal == false && charValue1.Count() > 0
                    ));
            }
        }

        private ICommand bDiv;
        public ICommand BDiv
        {
            get
            {

                return bDiv ?? (bDiv = new RelayCommand(
                                        p =>
                                        {
                                            actVal = true;
                                            CharOperator = '/';
                                        },
                                        p => actVal == false && charValue1.Count() > 0
                    ));
            }
        }

        private ICommand bProc;
        public ICommand BProc
        {
            get
            {

                return bProc ?? (bProc = new RelayCommand(
                                        p =>
                                        {
                                            actVal = true;
                                            CharOperator = '%';
                                        },
                                        p => actVal == false && charValue1.Count() > 0
                    ));
            }
        }


        private ICommand bMult;
        public ICommand BMult
        {
            get
            {

                return bMult ?? (bMult = new RelayCommand(
                                        p =>
                                        {
                                            actVal = true;
                                            CharOperator = '∙';
                                        },
                                        p => actVal == false && charValue1.Count() > 0

                    )) ;
            }
        }
        


    }

    }

